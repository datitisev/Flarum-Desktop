const { app, BrowserWindow } = require('electron');
const settings = require('electron-settings');

// Set up default settings
settings.defaults({
  forumURL: 'https://discuss.flarum.org',
});

let win, splashWin;

const createWindow = function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1000,
    height: 750,
    show: true,
    frame: true
  });

  // and load the index.html of the app.
  win.loadURL(`file://${__dirname}/static/index.html`);

  // Listen for a crash
  win.webContents.on('crash', console.error);

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  win.on('unresponsive', () => {
    dialog.showMessageBox(win, {
      type: 'error',
      buttons: [
        'Wait',
        'Close',
        'Reload'
      ],
      defaultId: 0,
      title: 'App is unresponsive',
      message: 'The app has become unresponsive. Do you want to reload the page?',
      detail: 'This error occurs when the web view hangs, most commonly because of JavaScript',
      cancelId: 0,
    }, index => {
      if (index == 1) win.close();
      else if (index == 2) win.reload();
    });
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit();
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});
